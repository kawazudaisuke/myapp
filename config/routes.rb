Rails.application.routes.draw do
  root to: 'ideas#toppage'

  resources :ideas do
    member do
      post "follow"
      delete "unfollow"
      post "like"
      delete "dislike"
    end
    collection do
      get 'toppage'
    end
  end

  resources :users do
  end

  get '/login', to: "session#new"
  post '/login', to: 'session#create'
  delete '/logout', to: 'session#destroy'
end
