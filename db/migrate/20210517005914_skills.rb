class Skills < ActiveRecord::Migration[5.2]
  def change
    add_reference :skills, :classification_of_skill, foreign_key: true
  end
end
