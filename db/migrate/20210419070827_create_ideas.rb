class CreateIdeas < ActiveRecord::Migration[5.2]
  def change
    create_table :ideas do |t|
      t.string :ideaname
      t.string :description
      t.integer :owner_id

      t.timestamps
    end
  end
end
