class CreateClassificationOfIdeas < ActiveRecord::Migration[5.2]
  def change
    create_table :classification_of_ideas do |t|
      t.string :idea_class
      t.timestamps
    end
  end
end
