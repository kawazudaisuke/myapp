class AddIndexToJoining < ActiveRecord::Migration[5.2]
  def change
    add_index :joinings, [:user_id, :idea_id], unique: true
  end
end
