class AddColumnToSkillIdea < ActiveRecord::Migration[5.2]
  def change
    add_column :skill_ideas, :skill_id, :integer
  end
end
