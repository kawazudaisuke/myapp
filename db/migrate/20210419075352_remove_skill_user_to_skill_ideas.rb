class RemoveSkillUserToSkillIdeas < ActiveRecord::Migration[5.2]
  def change
    remove_column :skill_ideas, :skill_user
  end
end
