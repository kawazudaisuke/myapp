class CreateSkillIdeas < ActiveRecord::Migration[5.2]
  def change
    create_table :skill_ideas do |t|
      t.integer :skill_user
      t.integer :idea_id

      t.timestamps
    end
  end
end
