class CreateClassificationOfSkills < ActiveRecord::Migration[5.2]
  def change
    create_table :classification_of_skills do |t|
      t.string :skill_class
      t.timestamps
    end
  end
end
