class RemoveUserIdeaToSkillIdeas < ActiveRecord::Migration[5.2]
  def change
    remove_column :skill_ideas, :user_id
  end
end
