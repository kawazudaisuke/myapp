class CreateSkillUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :skill_users do |t|
      t.integer :skill_id
      t.integer :user_id

      t.timestamps
    end
  end
end
