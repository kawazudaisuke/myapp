class AddColumnToIdeas < ActiveRecord::Migration[5.2]
  def change
    add_reference :ideas, :classification_of_idea, foreign_key: true
  end
end
