require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { create(:user) }

  describe "create new user" do
    context "in case of creation of new user successed" do
      it "is valid with name, email" do
        user = build(:user)
        expect(user).to be_valid
      end
    end

    context "in case of creation of new user failed" do
      it "is invalid without name" do
        user = build(:user, name: nil)
        user.valid?
        expect(user.errors[:name]).to include("名前を入力してください")
      end

      it "is invalid without email" do
        user = build(:user, mail: nil)
        user.valid?
        expect(user.errors[:mail]).to include("メールアドレスを入力してください")
      end

      it "is invalid without password" do
        user = build(:user, password: nil, password_confirmation: nil)
        user.valid?
        expect(user.errors[:password]).to include("パスワードは１０文字以上で登録してください")
      end

      it "is invalid with duplicate email" do
        create(:user, mail: "test@gmail.com")
        user = build(:user, mail: "test@gmail.com")
        user.valid?
        expect(user.errors[:mail]).to include("既に登録されているアドレスです")
      end

      it "is invalid that password and password_confirmation are not equal" do
        user = build(:user, password: "daisuke", password_confirmation: "kawazu")
        expect(user).to_not be_valid
      end
    end
  end
end
