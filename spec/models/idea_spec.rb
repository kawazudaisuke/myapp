require 'rails_helper'

RSpec.describe Idea, type: :model do
  it "is invalid without idea's name" do
    idea = build(:idea, ideaname: nil)
    idea.valid?
    expect(idea.errors[:ideaname]).to include("アイデア名を入力してください")
  end
  it "is invalid without description" do
    idea = build(:idea, description: nil)
    idea.valid?
    expect(idea.errors[:description]).to include("説明文を入力してください")
  end
end
