require 'rails_helper'
require 'capybara-screenshot/rspec'
MAX_DISPLAY_OF_IDEAS = 20
MAX_DISPLAY_OF_SKILLS = 5

RSpec.feature "ideas", type: :feature do
  let!(:owner) { create(:user) }
  let!(:classification_of_idea) { create(:classification_of_idea) }
  let!(:user) { create(:user) }
  let!(:ideas) { create_list(:idea, MAX_DISPLAY_OF_IDEAS, owner: owner) }
  let!(:idea) { create(:idea, owner: owner) }
  let!(:skills) { create_list(:skill, MAX_DISPLAY_OF_SKILLS, users: [user], ideas: [idea]) }

  before do
    visit login_path
    fill_in "session_mail", with: owner.mail.to_s
    fill_in "session_password", with: owner.password.to_s
    click_button "ログイン"
  end

  describe "test about Idea #index" do
    before do
      visit ideas_path
      click_on "早速始める"
    end
    scenario "top_page shows 20 items" do
      expect(page).to have_content("Test_idea", count: 20)
      expect(page).to_not have_content("Test_idea", count: 21)
    end
  end

  describe "Test Idea #SHOW" do
    before do
      visit idea_path(id: idea.id)
    end
    scenario "idea's name and description are shown correctly" do
      expect(page).to have_content idea.ideaname
      expect(page).to have_content idea.description
    end
    scenario "skill's names are shown" do
      skills.each_index do |n|
        expect(page).to have_content skills[n].skillname
      end
    end
  end

  describe "user can make new idea #NEW" do
    scenario "can make new idea" do
      visit new_idea_path
      fill_in "idea_ideaname", with: "New Idea"
      fill_in "idea_description", with: "This is New Idea"
      skills.each_index do |n|
        check skills[n].skillname
      end
      click_on "新規登録"
      expect(page).to have_http_status(200)
    end
    scenario "expecting on rendering New when it false to make new idea" do
      visit new_idea_path
      fill_in "idea_ideaname", with: ""
      fill_in "idea_description", with: "This is New idea"
      click_on "新規登録"
      expect(page).to have_content "アイデア新規登録"
    end
  end

  describe "can edit idea　#EDIT" do
    before do
      visit edit_idea_path(id: idea.id)
    end
    scenario "entities of the idea are shown" do
      expect(find_field('idea_ideaname').value).to eq idea.ideaname
      expect(find_field('idea_description').value).to eq idea.description
      skills.each_index do |n|
        expect(page).to have_content skills[n].skillname
      end
    end
    scenario "idea can be destroyed" do
      click_on "アイデア削除"
      expect(page).to have_http_status(200)
    end
  end

  scenario "search works correctly" do
    fill_in "q_ideaname_cont", with: "Test_idea"
    click_on "SEARCH"
    within("#wrap") do
      expect(page).to have_content "Test_idea"
    end
  end
end
