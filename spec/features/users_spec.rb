require 'rails_helper'
require 'capybara-screenshot/rspec'

RSpec.feature "users", type: :feature do
  let(:user_a) { create(:user) }
  let(:user_b) { create(:user, name: "Test User B", mail: "testuserB@gmail.com") }
  let!(:owner) { create(:user) }
  let!(:ideas) { create_list(:idea, 10, owner: owner) }
  let!(:idea_b) { create(:idea, ideaname: "IDEA USER B", owner: user_b) }
  let!(:joining) { create(:joining, idea_id: idea_b.id, user_id: user_a.id) }
  let!(:skill) { create(:skill, skillname: "SkillA") }
  let!(:skill_user) { create(:skill_user, skill_id: skill.id, user_id: user_a.id) }

  describe "CRUD funciton of User can work" do
    context "new account can be made #NEW" do
      scenario "can make new account" do
        visit new_user_path
        fill_in "user_name", with: "New User"
        fill_in "user_mail", with: "newuser@gmail"
        fill_in "user_password", with: "newuserpassword"
        fill_in "user_password_confirmation", with: "newuserpassword"
        expect(page).to have_http_status(200)
      end
      scenario "cannot make new account" do
        visit new_user_path
        fill_in "user_name", with: "New User"
        fill_in "user_mail", with: "newuser@gmail"
        fill_in "user_password", with: "newuserpassword"
        fill_in "user_password_confirmation", with: "user"
        expect(page).to have_content "新規登録"
      end
    end

    context "UserA logged in" do
      before do
        visit login_path
        fill_in "session_mail", with: user_a.mail.to_s
        fill_in "session_password", with: user_a.password.to_s
        click_button "ログイン"
      end

      scenario "shows userA's #SHOW" do
        visit user_path(id: user_a.id)
        expect(page).to have_content "IDEA USER B"
        expect(page).to have_content "SkillA"
      end

      scenario "shows current_user #EDIT" do
        visit edit_user_path(id: user_a.id)
        expect(find_field('user_name').value).to eq 'Test User A'
        expect(find_field('user_mail').value).to eq user_a.mail.to_s
        expect(page).to have_checked_field "SkillA"
      end

      scenario "can edit current_user's account #UPDATE" do
        visit edit_user_path(id: user_a.id)
        fill_in "user_name", with: "NEW Test user A"
        fill_in "user_mail", with: "NEWtestuserA@gmail.com"
        uncheck "SkillA"
        click_button "更新"
        expect(page).to have_http_status(200)
      end

      scenario "can destory account #DESTROY" do
        visit user_path(id: user_a.id)
        click_link "アカウント削除"
        expect(page).to have_http_status(200)
      end
    end
  end
  # scenario "UserB logged in" do
  #   visit login_path
  #   user_b = create(:user, name:"Test user B", mail:"testuserB@gmail.com", password:"testuserb")
  #   fill_in "session_mail", with: user_b.mail
  #   fill_in "session_password", with: user_b.password
  #   click_button "ログイン"
  # end

  # describe " test user #Show " do
  #   before do
  #     visit user_path(id: user.id)
  #   end
  #   scenario " shows users details " do
  #     my_ideas.each_index do |n|
  #       expect(page).to have_content my_ideas[n].ideaname
  #       expect(page).to have_content my_ideas[n].description
  #     end
  #     followings.each_index do |n|
  #       expect(page).to have_content followings[n].ideaname
  #       expect(page).to have_content followings[n].description
  #     end
  #     skills.each_index do |n|
  #       binding.pry
  #       expect(page).to have_content skills[n].skillname
  #     end
  #   end
  #
  # end
end
