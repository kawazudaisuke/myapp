FactoryBot.define do
  factory :skill do
    sequence(:skillname) { |n| "Skill_#{n}" }
  end
end
