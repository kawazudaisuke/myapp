FactoryBot.define do
  factory :user do
    name { "Test User A" }
    introduction { "Hello, I am test user A" }
    sequence(:mail, "testuserA@gmail.com")
    password { "testuserpassword" }
  end
end
