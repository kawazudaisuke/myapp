FactoryBot.define do
  factory :idea do
    sequence(:ideaname) { |n| "Test_idea_#{n}" }
    description { "This is test_idea" }
  end
end
