class ClassificationOfIdea < ApplicationRecord
  has_many :ideas, foreign_key: :class_id
end
