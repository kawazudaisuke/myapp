class Like < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :idea, optional: true

  validates :user_id, uniqueness: { scope: :idea_id }
end
