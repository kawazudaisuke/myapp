class Skill < ApplicationRecord
  has_many :skill_users
  has_many :users, through: :skill_users

  has_many :skill_ideas
  has_many :ideas, through: :skill_ideas
end
