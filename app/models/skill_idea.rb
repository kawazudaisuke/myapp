class SkillIdea < ApplicationRecord
  belongs_to :idea
  belongs_to :skill
end
