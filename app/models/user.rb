class User < ApplicationRecord
  has_secure_password
  validates :name, presence: { message: "名前を入力してください" }
  validates :mail, presence: { message: "メールアドレスを入力してください" }, uniqueness: { message: "既に登録されているアドレスです" }
  validates :password, presence: { message: "パスワードを入力してください" }, length: { minimum: 10, message: "パスワードは１０文字以上で登録してください" }, on: :create

  has_many :ideas, foreign_key: :owner_id, dependent: :destroy

  has_many :joinings
  has_many :ideas, through: :joinings, dependent: :destroy

  has_many :skill_users
  has_many :skills, through: :skill_users, dependent: :destroy

  has_many :likes
  has_many :ideas, through: :likes, dependent: :destroy

  has_one_attached :avatar, dependent: :destroy
end
