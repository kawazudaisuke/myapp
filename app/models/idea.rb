class Idea < ApplicationRecord
  validates :ideaname, presence: { message: "アイデア名を入力してください" }
  validates :description, presence: { message: "説明文を入力してください" }

  belongs_to :owner, class_name: 'User', foreign_key: :owner_id
  belongs_to :classification_of_idea

  has_many :joinings
  has_many :users, through: :joinings, dependent: :destroy

  has_many :skill_ideas
  has_many :skills, through: :skill_ideas, dependent: :destroy

  has_many :likes
  has_many :users, through: :likes, dependent: :destroy

  has_one_attached :image, dependent: :destroy

  def self.ransackable_attributes(_auth_object = nil)
    %w[ideaname]
  end

  def self.ransackable_associations(_auth_object = nil)
    []
  end

  def liked_by?(user_id)
    likes.where(user_id: user_id).exists?
  end

  scope :limit_display, -> { limit(20).reverse_order }
end
