class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user

  private

  def current_user
    @current_user ||= User.find_by(id: session[:user_id]) if session[:user_id]
  end

  # 共通分部分である検索窓に関連して「＠ｑのインスタンスを生成していない」とエラー文が出たので追記
  def ransackable
    @q = Idea.ransack(params[:q])
  end
end
