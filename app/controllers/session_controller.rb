class SessionController < ApplicationController
  before_action :ransackable

  def new; end

  def create
    @user = User.find_by(mail: session_params[:mail])
    if @user&.authenticate(session_params[:password])
      session[:user_id] = @user.id
      flash[:notice] = "ログインしました"
      redirect_to ideas_path
    else
      render :new
    end
  end

  def destroy
    reset_session
    redirect_to root_url
  end

  private

  def session_params
    params.require(:session).permit(:mail, :password)
  end
end
