class UsersController < ApplicationController
  before_action :ransackable
  #ログインしていない場合はトップページへとリダイレクト
  before_action except: [:new, :create] do
    redirect_without_login
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:notice] = "新規ユーザが登録されました"
      session[:user_id] = @user.id
      redirect_to ideas_path
    else
      flash.now[:notice]
      render :new
    end
  end

  def show
    @my_ideas = Idea.where(owner_id: params[:id])
    @my_ideas.each do |myidea|
      @followers = myidea.users
    end
    @user = User.find(params[:id])
    @skills = @user.skills
  end

  # 自分が作成したアイデアの表示 MY IDEAS
  def show_idea
    @my_ideas = Idea.where(owner_id: params[:id])
  end

  def edit
    @user = User.find(current_user.id)
    @skills = Skill.all
  end

  def update
    @user = User.find(current_user.id)
    @user.update(user_params)
    if @user.registered == 1
      #登録後の初回のみ表示されるインストラクションでボタンを押した時のみ
      render template: "idea/index"
    else
      #通常のユーザアカウントの更新の場合
      redirect_to user_path(current_user.id)
    end
  end

  def destroy
    @user = User.find(current_user.id)
    @user.destroy
    redirect_to root_url
  end

  private

  def user_params
    params.require(:user).permit(:name, :mail, :password, :password_confirmation, :introduction, :avatar, :registered, skill_ids: [])
  end

  def redirect_without_login
    redirect_to root_url if current_user.nil?
  end
end
