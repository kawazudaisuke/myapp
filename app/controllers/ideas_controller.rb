class IdeasController < ApplicationController
  before_action :ransackable

  def index
    # limit_displayは最大表示に関するファンクション
    @ideas = @q.result(distinct: true).limit_display
    @user = User.find(current_user.id)
  end

  # 　新たに他ユーザのアイデアをフォローする場合
  def follow
    @followings = current_user.joinings.new(idea_id: params[:id])
    @idea = Idea.find(params[:id])
    @owner = User.find(@idea.owner_id)
    @follower = User.find(current_user.id)
    if @followings.save
      IdeaMailer.follower_email(@follower, @owner).deliver_now
      redirect_to root_url
    else
      flash[:notice] = "#{@owner.name}を二重にフォローできません"
      redirect_to idea_path(params[:id])
    end
  end

  def unfollow
    @joining = Joining.find_by(user_id: current_user.id, idea_id: params[:id])
    @joining.delete
  end

  def like
    @likes = current_user.likes.new(idea_id: params[:id])
    @idea = Idea.find(params[:id])
    respond_to :js if @likes.save
  end

  def dislike
    @like = Like.find_by(user_id: current_user.id, idea_id: params[:id])
    @idea = Idea.find(params[:id])
    respond_to :js if @like.destroy
  end

  # アイデアの詳細の表示
  def show
    @idea = Idea.find_by(id: params[:id])
    @user = User.find(@idea.owner_id)
    @skills = @idea.skills
    @classification_of_idea = @idea.classification_of_idea
  end

  # ログイン中ユーザが新規でアイデアを作成
  def new
    @skills = Skill.all
    @classification_of_ideas = ClassificationOfIdea.all
  end

  def create
    @idea = Idea.new(idea_params)
    if @idea.save
      redirect_to ideas_path
    else
      render :new
    end
  end

  # ABOUTUSの中身
  def toppage; end

  def edit
    @idea = Idea.find(params[:id])
    @skills = Skill.all
    @classification_of_ideas = ClassificationOfIdea.all
  end

  def update
    @idea = Idea.find(params[:id])
    @idea.update(idea_params)
    redirect_to root_url
  end

  def destroy
    @idea = Idea.find(params[:id])
    @idea.destroy
    redirect_to root_url
  end

  private

  def idea_params
    params.require(:idea).permit(:ideaname, :description, :owner_id, :image, :classification_of_idea_id, skill_ids: [])
  end
end
