class ApplicationMailer < ActionMailer::Base
  default from: 'follower@example.com'
  layout 'mailer'
end
