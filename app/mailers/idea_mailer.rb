class IdeaMailer < ApplicationMailer
  def follower_email(follower, owner)
    @follower = follower
    @owner = owner
    mail(
      subject: "フォロー通知メール",
      to: @owner.mail,
      from: @follower.mail
    )
  end
end
