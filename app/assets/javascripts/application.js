// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require activestorage
//= require jquery.turbolinks
//= require turbolinks
//= require_tree .
//= require masonry/jquery.masonry



// ================トップページ関係==================
$(window).on('scroll', function () {
  var loginBox = $('.login-modal-wrapper')
  loginBox.css('display','none');
  var doch = $(document).innerHeight();
  var winh = $(window).innerHeight();
  var bottom = doch - winh;
    if (bottom * 0.93 <= $(window).scrollTop()) {
    //一番下までスクロールした時に実行
    loginBox.fadeIn();
    }
  });

// クリックをしたらモーダルを消す
$(window).on('load',function(){
  $(document).on('click', function(){
    var loginBox = $('.login-modal-wrapper')
    loginBox.fadeOut();
  })
});

// ===========Index関連=============

// スクロールを禁止するファンクション
function disableScroll(event) {
  event.preventDefault();
}

$(window).one('load',function(){
  var instruction = $('#instruction-modal')
  instruction.fadeIn();
  if (instruction.css('display') == "block"){
    document.addEventListener('touchmove', disableScroll, { passive: false });
    document.addEventListener('mousewheel', disableScroll, { passive: false });
  }else{
    document.removeEventListener('touchmove', disableScroll, { passive: false });
    document.removeEventListener('mousewheel', disableScroll, { passive: false });
  }
    $('#close-button').on('click',function(){
      instruction.fadeOut();
      document.removeEventListener('touchmove', disableScroll, { passive: false });
      document.removeEventListener('mousewheel', disableScroll, { passive: false });
    })
});

// バブリング現象回避の為、mouseoverではなくhoverを使用
$(window).on('load',function(){
  var description = $('.idea-description-modal')
  $('.item_container').hover(function(){
    $(this).children('.idea-description-modal').fadeIn();
  }, function(){
    $(this).children('.idea-description-modal').fadeOut();
  });
});

// ===========スクロールした際の画像の効果　トップページで使用=============
$(window).on('load',function(){
  $(function(){
    ScrollReveal().reveal('.scroll-animation',{
      duration:1500,
      delay: 200,
      viewFactor: 0.2
    });
  });
});

// ===========Maisonry.railsに関する事項=============

$(window).on('load',function(){
  $('#wrap').masonry({
    itemSelector: '.item_container',
    columnWidth: 200,
    gutterWidth: 10
  });
});

// --------アイデアの表示サイズに関する事項---------
document.addEventListener('turbolinks:load',function(){
  $('.item_container.0').addClass('item_big');
  $('.item_container.1').addClass('item_middle');
  $('.item_container.3').addClass('item_middle');
  $('.item_container.10').addClass('item_middle');
  $('.item_container.15').addClass('item_middle');
});

// --------アイデアの分類毎のアイコン表示------------
document.addEventListener('turbolinks:load',function(){
  $('.idea-class.1').addClass('fund');
  $('.idea-class.2').addClass('event');
  $('.idea-class.3').addClass('business');
  $('.idea-class.4').addClass('creative');
  $('.idea-class.3').addClass('idea');
  $('.idea-class.4').addClass('volunteer');
});

// ===========フォローしたアイデアの削除（AJAX）=============

document.addEventListener('turbolinks:load',function(){
  document.querySelectorAll('.follow-delete-btn').forEach(function(a){
    a.addEventListener('ajax:success', function(){
      var td = a.parentNode;
      var tr = td.parentNode;
      tr.style.display = 'none';
    });
  });
});

// ===========メニュー画面のスライド表示の効果=============

document.addEventListener("turbolinks:load", function() {
  $('#open-nav').on('click', function(){
    $('.menu-container').addClass('show');
    $('#open-nav').css('display','none');
  });
  $('#close-nav').on('click', function(){
    $('.menu-container').removeClass('show');
    $('#open-nav').css('display','block');
  });
});
