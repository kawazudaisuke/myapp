module ApplicationHelper
  def html_title(title = "")
    if title.empty?
      "SEEDS"
    else
      "#{title} - SEEDS"
    end
  end
end
