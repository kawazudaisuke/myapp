
1. Webサービス「SEED」について
  -  サイトの目的
     - 誰しも持っている『漠然としたアイデア』のを実現する為に必要な、さらなるアイデアと知識、技術等を有する人間同士を結びつけることを支援するサイトです。
  -  作成のきっかけ
     - アイデア実現を支援するサービスは、クラウドファンディングがメジャーですが、アイデアを実現化する為には金銭面以外にも様々な乗り越えなければならないキャズムが存在します。
     -  そうした、アイデアの想起と実現の間に横たわるキャズムを克服する為に必要な、さらなるアイデアや専門的知識、専門技術といった様々な要素を有する人々にユーザーがアプローチすることを支援できないものかと考えたのがきっかけです。

![トップページ](https://bitbucket.org/kawazudaisuke/myapp/raw/409de3fc3145bdd38d821a013a627c99ee9ad6bb/app/assets/images/top-image.png)

  -  何ができるのか
     - 自らアイデアとそのアイデア実現に必要なスキルを投稿し、賛同者を募ることができます。
     - ヘッダーの検索窓からIdeaを検索できます。
     - 他のユーザが投稿したアイデアをフォローすることもできます。
     - 他のユーザからフォローされると、自動的にフォローされたことが通知メールで届きます。

  -  アピールポイント
     - 　Masonry railsを活用してインパクトかつ動的な表現を可能にしました。
     - 　Ajaxにより、「いいね」機能及びフォローしたアイデアの画面遷移なしの削除を可能にしました。
     -  CircleCiを活用することにより継続的なデプロイの自動化を可能にしました。
     -  初回登録時のみに発生するモーダルによるガイダンスを追加してUXを向上しました。
---

2. 使用技術等

  -  Ruby 2.5.3

  -  Rails 5.2.5

  -  MySQL 5.7

  -  Docker/Docker-Compose

  -  CircleCi CI/CD

  -  AWS
     - S3（画像の保存先として使用）

  -  Heroku（アプリのデプロイ）

  -  RSpec

  -  rubocop 0.76.0

  -  masonry-rails 0.2.4 (アイデア一覧のブロック状の表示に使用)

  -  ransack 2.4.1（検索機能）

---

![モデルの構成図](https://bitbucket.org/kawazudaisuke/myapp/raw/409de3fc3145bdd38d821a013a627c99ee9ad6bb/app/assets/images/relations.png)



3. AWS(インフラ)構成図

![インフラ図](https://bitbucket.org/kawazudaisuke/myapp/raw/409de3fc3145bdd38d821a013a627c99ee9ad6bb/app/assets/images/infra.png)

4. 機能、非機能一覧
  -  機能

     - ユーザー登録・削除機能

     - 検索機能（ransack使用）

     - 画像投稿

     - フォロー/フォロワー機能

     - フォロー削除（Ajax）

  -  テスト
     -  モデルテスト（Model）

     -  統合テスト（Feature）

5. サービス利用について

    ![ログイン・登録画面](https://bitbucket.org/kawazudaisuke/myapp/raw/409de3fc3145bdd38d821a013a627c99ee9ad6bb/app/assets/images/login-signup.png)

  -  ログイン画面の「ゲストログイン」から新規登録無しで入ることができます。

    ![メニュー](https://bitbucket.org/kawazudaisuke/myapp/raw/409de3fc3145bdd38d821a013a627c99ee9ad6bb/app/assets/images/header.png)

    ![アイデア詳細画面](https://bitbucket.org/kawazudaisuke/myapp/raw/409de3fc3145bdd38d821a013a627c99ee9ad6bb/app/assets/images/idea-show.png)

  -  その他
     -  アカウント及び自分のアイデアはそれぞれの詳細画面から編集可能です。
     -  自らのアイデアへのフォロワーについては、マイアカウントから確認可能です。
